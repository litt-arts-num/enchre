import OpenSeadragon from "openseadragon";

$(function () {
  let viewerLeft = createViewer("seadragon-viewer-left", "left")
  let viewerRight = createViewer("seadragon-viewer-right", "right")
  decorate()
})

// gestion des touches gauche et droite pour passer à la page précédente/suivante
$(document).keydown(function (e) {
  switch (e.which) {
    case 37:
      let previous = document.getElementById('previous-btn')
      if (previous) {
        window.location = previous.dataset.absolute
      }
      break;

    case 39:
      let next = document.getElementById('next-btn')
      if (next) {
        window.location = next.dataset.absolute
      }
      break;

    default:
      return;
  }
  e.preventDefault();
});

// gestion des numéros de lignes à afficher/masquer
var btnToggleLineNumber = ".toggle-linenumbers-btn";
if ($(btnToggleLineNumber).length) {
  $(btnToggleLineNumber).click(function () {
    var container = $(this).data("container");
    var classToToggle = "numline";
    var select = container + " ." + classToToggle;
    $(select).toggle();
  })
}

// gestion de la modale pour les notes
$('.note-call').click(function () {
  var id = $(this).data("note-id");
  var el = $("[data-note-title-id='" + id + "']").first();
  let isNotice = (el.parent().css("display") == "none")
  var modale = (isNotice) ? $("#noticeModale") : $("#noteModale");
  var title = $("[data-note-title-id='" + id + "']").first().text();
  var text = $("[data-note-content-id='" + id + "']").first().html();

  modale.find(".modal-body").html(text);
  modale.find(".modal-title").html(title);
  modale.modal('show');
});

$('.reset-style-btn').click(function () {
  var pos = $(this).data("pos");
  var container = $(this).data("container");
  var className = $(this).data("classname");
  var select = container + " mark." + className;
  $(select).toggleClass("reset-styles");
})

const decorate = () => {
  let hash = $(location).attr('hash')
  if (hash) {
    document.querySelectorAll('span[data-id-notion="' + hash + '"]').forEach(el => {
      el.classList.add("decorated")
    });
  }
}

// Initialisation d'OpenSeadragon
const createViewer = (id, prefix) => {
  let url = document.getElementById('imageURL').value
  let viewer = new OpenSeadragon.Viewer({
    id: id,
    showNavigator: false,
    showRotationControl: true,
    prefixUrl: 'node_modules/openseadragon/build/openseadragon/images',
    zoomInButton: prefix + '-osd-zoom-in',
    zoomOutButton: prefix + '-osd-zoom-out',
    homeButton: prefix + '-osd-home',
    fullPageButton: prefix + '-osd-full-page',
    rotateLeftButton: prefix + '-osd-left',
    rotateRightButton: prefix + '-osd-right',
    tileSources: {
      type: 'image',
      url: url
    }
  })
  // disable keyboard shortcuts
  viewer.innerTracker.keyHandler = null
  viewer.addHandler('open', function () {
    var viewportBounds = viewer.viewport.getBounds();
    var viewportAspect = viewportBounds.height / viewportBounds.width;
    var newBounds = viewer.world.getHomeBounds();
    newBounds.height = newBounds.width * viewportAspect;
    viewer.viewport.fitBounds(newBounds);
  });

  return viewer
}