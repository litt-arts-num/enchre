let input = document.getElementById("filter-notice")
let entries = document.getElementsByClassName("notice-entry");
let entriesArray = Array.from(entries)
let letterBlocks = document.getElementsByClassName("letter-block");
let letterBlocksArray = Array.from(letterBlocks)

input.addEventListener("keyup", function() {
  filter(this.value)
}, false);

function filter(value) {
  entriesArray.forEach(function(entry) {
    entry.style.display = (entry.innerHTML.toLowerCase().indexOf(value.toLowerCase()) > -1) ? "block" : "none"
  })

  letterBlocksArray.forEach(function(block) {
    // duplicate the selector, with and without space cause some browsers normalize it.
    block.style.display = block.querySelector("li:not([style*='display: none']):not([style*='display:none'])") ? "block" : "none"
  })
}
