<?php

namespace App\Form;

use App\Entity\Cahier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('notes_annotations_et_noticeBio', FileType::class, [
                'label' => 'Fichier HR_Notes_Annotations_et_NoticeBio.xml',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/xml'],
            ])
            ->add('dictionnaire', FileType::class, [
                'label' => 'Fichier HR_dictionnaire.xml',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/xml'],
            ])
            ->add('formes_normalisees', FileType::class, [
                'label' => 'Fichier HR_formes_normalisees.xml',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/xml'],
            ])
            ->add('save', SubmitType::class, [
              'label' => 'Importer',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cahier::class,
        ]);
    }
}
