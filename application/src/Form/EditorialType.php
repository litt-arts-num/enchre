<?php

namespace App\Form;

use App\Entity\Cahier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditorialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projet_corpus', FileType::class, [
                'label' => 'Fichier XML "Projet & Corpus"',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/xml'],
            ])
            ->add('principes_edition', FileType::class, [
                'label' => 'Fichier XML "Principes d\'édition"',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/xml'],
            ])
            ->add('equipe_partenaires', FileType::class, [
                'label' => 'Fichier XML "Équipe"',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/xml'],
            ])
            ->add('biblio', FileType::class, [
                'label' => 'PDF Bibliographie',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/pdf'],
            ])
            ->add('table_cahiers', FileType::class, [
                'label' => 'PDF Table des cahiers',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/pdf'],
            ])
            ->add('chronologie', FileType::class, [
                'label' => 'PDF Chronologie',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept' => 'application/pdf'],
            ])
            ->add('save', SubmitType::class, [
              'label' => 'Importer',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cahier::class,
        ]);
    }
}
