<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TextSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('request', TextType::class, [
                'required' => true,
                'mapped' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Requête'
                ]
            ])
            ->add('search_in', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' => [
                    'Transcription linéarisée' => 'linearized',
                    'Transcription semi-diplomatique' => 'pseudodiplo',
                    'Annotations' => 'notes'
                ],
                'data' => ['linearized', 'pseudodiplo', 'notes'],
                'expanded' => true,
                'multiple' => true
            ])

            ->add('case_sensitive', CheckboxType::class, [
                'label' => 'Sensible à la casse',
                'required' => false,
                'attr' => [
                    'checked' => false
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Rechercher',
            ]);
    }
}
