<?php

namespace App\Form;

use App\Entity\Cahier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CahierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          /*
            ->add('splitPageFile', FileType::class, [
                'label' => 'XML (split par page)',
                'mapped' => false,
                'required' => true,
                'attr' => ['accept' => 'application/xml'],
            ])
            */
            ->add('teiFile', FileType::class, [
                'label' => 'Fichier XML-TEI du cahier',
                'mapped' => false,
                'required' => true,
                'attr' => ['accept' => 'application/xml'],
            ])
            ->add('save', SubmitType::class, [
              'label' => 'Importer',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cahier::class,
        ]);
    }
}
