<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CahierRepository")
 */
class Cahier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Page", mappedBy="cahier", cascade={"all"})
     */
    private $pages;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $xmlFileName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $teiFileName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slugName;

    /**
     * @ORM\Column(type="boolean")
     */
    private $public = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dates;

    /**
     * @ORM\Column(type="boolean")
     */
    private $wip = false;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setCahier($this);
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        if ($this->pages->contains($page)) {
            $this->pages->removeElement($page);
            // set the owning side to null (unless already changed)
            if ($page->getCahier() === $this) {
                $page->setCahier(null);
            }
        }

        return $this;
    }

    public function getXmlFileName(): ?string
    {
        return $this->xmlFileName;
    }

    public function setXmlFileName(string $xmlFileName): self
    {
        $this->xmlFileName = $xmlFileName;

        return $this;
    }

    public function getTeiFileName(): ?string
    {
        return $this->teiFileName;
    }

    public function setTeiFileName(string $teiFileName): self
    {
        $this->teiFileName = $teiFileName;

        return $this;
    }

    public function getSlugName(): ?string
    {
        return $this->slugName;
    }

    public function setSlugName(string $slugName): self
    {
        $this->slugName = $slugName;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(?string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getDates(): ?string
    {
        return $this->dates;
    }

    public function setDates(?string $dates): self
    {
        $this->dates = $dates;

        return $this;
    }

    public function getWip(): ?bool
    {
        return $this->wip;
    }

    public function setWip(bool $wip): self
    {
        $this->wip = $wip;

        return $this;
    }
}
