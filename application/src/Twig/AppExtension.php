<?php

namespace App\Twig;

use App\Entity\Cahier;
use App\Entity\EditorialContent;
use App\Entity\Page;
use App\Manager\XmlManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $em;
    private $xmlManager;
    
    public function __construct(EntityManagerInterface $em, XmlManager $xmlManager)
    {
        $this->em = $em;
        $this->xmlManager = $xmlManager;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('getCahiers', [$this, 'getCahiers']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getCahiers', [$this, 'getCahiers']),
            new TwigFunction('getAllCahiers', [$this, 'getAllCahiers']),
            new TwigFunction('getNext', [$this, 'getNext']),
            new TwigFunction('getHTML', [$this, 'getHTML']),
            new TwigFunction('getNotice', [$this, 'getNotice']),
        ];
    }

    public function getAllCahiers($isAdmin) {
        $cahiers =  [
            ["Cahier IV", "cahier IV", "(7 févr. 1887 – 22 avr. 1887)"],
            ["Cahier V", "cahier V", "(23 avr. 1887 – 1er oct. 1887)"],
            ["Cahier VI", "cahier VI", "(1er oct. 1887 – 15 mars 1888)"],
            ["Cahier VII", "cahier VII", "(15 mars 1888 – 1er août 1888)"],
            ["Cahier VIII", "cahier VIII", "(2 août 1888 – 1er décembre 1888)"],
            ["Cahier IX", "cahier IX", "(1er décembre 1888 – 1er avr. 1889)"],
            ["Cahier X", "cahier X", "(1er avr. 1889 – 7 août 1889)"],
            ["Cahier XI", "cahier XI", "(7 août 1889 – 9 févr. 1890)"],
            ["Cahier XII", "cahier XII", "(9 févr. 1890 – 4 nov. 1890)"],
            ["Cahier XIII", "cahier XIII", "(4 nov. 1890 – 1er mai 1891)"],
            ["Cahier XIV", "cahier XIV", "(1er mai 1891 – 1er déc. 1891)"],
            ["Cahier XV", "cahier XV", "(1er déc. 1891 – 24 mars 1892)"],
            ["Cahier XVI", "cahier XVI", "(25 mars 1892 – 15 sept. 1892)"],
            ["Cahier XVII", "cahier XVII", "(15 sept. 1892 – 22 févr. 1893)"],
            ["Cahier XVII bis", "cahier XVII bis", "(23 févr. 1893 – 2 avr. 1893)"],
            ["Cahier XVIII", "cahier XVIII", "(6 avr. 1893 – 15 juin 1893)"],
            ["Cahier XIX", "cahier XIX", "(15 juin 1893 – 3 nov. 1893)"],
            ["Cahier XX", "cahier XX", "(3 nov. 1893 – 1er févr. 1894)"],
            ["Cahier XXI", "cahier XXI", "(1er févr. 1894 – 7 mai 1894)"],
            ["Cahier XXII", "cahier XXII", "(7 mai 1894 – 1er août 1894)"],
            ["Cahier XXIII", "cahier XXIII", "(1er août 1894 – 1er janv. 1895)"],
            ["Cahier XXIV", "cahier XXIV", "(1er janv. 1895 – 10 mai 1895)"],
            ["Cahier XXV", "cahier XXV", "(10 mai 1895 – 15 novembre 1898)"],
            ["Cahier XXVI", "cahier XXVI", "(15 novembre 1898 – 1er janv. 1900)"],
            ["Notes", "notes", "(1er sept. 1900 – 22 août 1902)"],
            ["Notes", "notes", "(5 sept. 1902 – 20 déc. 1903)"],
            ["Journal", "journal", "(1er janv. 1904 – septembre 1905)"]
        ];

        $out = [];
        foreach ($cahiers as $cahier) {
            $name = $cahier[1];
            $dbCahier = $this->em->getRepository(Cahier::class)->findOneByName($name);
            if ($dbCahier && ($dbCahier->getPublic() || $isAdmin)) {
               $out[] = ["slug" => $dbCahier->getSlugName(), "name" => $dbCahier->getName(), "dates" => $dbCahier->getDates()];
            } else {
                $displayName = $cahier[0];
                $dates = $cahier[2];
                $out[] = ["slug"=> null, "name" => $displayName, "dates" => $dates];
            }
        }

        return $out;
    }

    public function getCahiers()
    {
        return $this->em->getRepository(Cahier::class)->findBy([], ["shortName" => "ASC"]);
    }

    public function getNext(Page $page)
    {
        return $this->em->getRepository(Page::class)->findOneByPrevious($page);
    }

    public function getHTML($what, Cahier $cahier, $pageName)
    {
        return $this->xmlManager->getHTML($what, $cahier, "page", $pageName);
    }

    public function getNotice(Cahier $cahier)
    {
        return $this->xmlManager->getHTML("edito", $cahier, "cahier");
    }
}
