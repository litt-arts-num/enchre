<?php

namespace App\Command;

use App\Manager\XmlManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CacheInvalidateCommand extends Command
{
    private $xm;
    protected static $defaultName = 'app:cache:invalidate';

    public function __construct(XmlManager $xm)
    {
        $this->xm = $xm;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Invalidate cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->xm->cacheInvalidate();
        $io->success('Cache invalidé');
    }
}
