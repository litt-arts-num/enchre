<?php

namespace App\Manager;

use \Saxon\SaxonProcessor;
use App\Entity\Cahier;
use App\Entity\Page;
use App\Manager\XmlManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Finder\Finder;

class ImportManager
{
    private $em;
    private $xmlManager;
    private $session;
    public function __construct(EntityManagerInterface $em, XmlManager $xmlManager, SessionInterface $session)
    {
        $this->em = $em;
        $this->xmlManager = $xmlManager;
        $this->session = $session;
    }


    public function getImages($dir)
    {
        $finder = new Finder();

        $finder->files()->in($dir);
        $images = [];
        foreach ($finder as $file) {
            $images[] = $file->getRelativePathname();
        }

        return $images;
    }

    public function import($baseName, $teiFileName, $uploadDir, $oldDir)
    {
        $saxonProc  = new SaxonProcessor();
        $xsltProc   = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($uploadDir.DIRECTORY_SEPARATOR.$teiFileName);
        $xslFile    = '/var/www/data/xslt/splitByPages.xsl';
        $xsltProc->compileFromFile($xslFile);
        $xml        = $xsltProc->transformToString();

        $splitFileName = $baseName . "-split.xml";
        file_put_contents($uploadDir.DIRECTORY_SEPARATOR.$splitFileName, $xml);

        $crawler = new Crawler();
        $crawler->addXmlContent($xml);
        $moreThanOneNs    = preg_match('/xmlns:/', $xml) ? true : false;
        $defaultNs        = ($moreThanOneNs === false) ? "" : "default:";
        $cahierName       = $crawler->filterXPath('//'.$defaultNs.'name[@type="cahier"]')->text();
        $cahierShortName  = $crawler->filterXPath('//'.$defaultNs.'name[@type="cahier"]/@xml:id')->text();

        if (!$cahier = $this->em->getRepository(Cahier::class)->findOneByShortName($cahierShortName)) {
            $cahier = new Cahier;
            $cahier->setName($cahierName);
            $cahier->setShortName($cahierShortName);
            $cahier->setTeiFileName($teiFileName);
            $cahier->setXmlFileName($splitFileName);
            $cahier->setSlugName($this->slugify($cahierShortName));

            $pages = $crawler->filterXPath('//'.$defaultNs.'div[@type="page"]')->each(function (Crawler $node, $i) use ($crawler, $cahier, $defaultNs) {
                $imageID      = ltrim($node->filterXPath('//'.$defaultNs.'pb')->attr("facs"), '#');
                $pageName     = $node->filterXPath('//'.$defaultNs.'fw[@type="num_page_ref"]')->text();
                $imageURL     = $crawler->filterXPath('//'.$defaultNs.'graphic[@xml:id="'.$imageID.'"]')->attr('url');
                $thumbnailURL = $this->xmlManager->getHTML("thumbnail", $cahier, "page", $pageName);

                return [$pageName, $imageURL, $thumbnailURL];
            });

            $previous = null;
            foreach ($pages as $p) {
                if (!$page = $this->em->getRepository(Page::class)->findOneBy(["cahier" => $cahier, "name" => $p[0] ])) {
                    $page = new Page;
                    $page->setPrevious($previous);
                    $page->setName($p[0]);
                    $page->setImage($p[1]);
                    $page->setThumbnail($p[2]);
                    $page->setCahier($cahier);
                    $previous = $page;

                    $this->em->persist($page);
                }
            }
            $message = "Nouveau cahier importé";
        } else {

            $oldTEI =  $cahier->getTeiFileName();
            $oldXML = $cahier->getXmlFileName();
            $filesystem = new Filesystem;
            $filesystem->rename($uploadDir.DIRECTORY_SEPARATOR.$oldTEI, $oldDir.DIRECTORY_SEPARATOR.$oldTEI);
            $filesystem->rename($uploadDir.DIRECTORY_SEPARATOR.$oldXML, $oldDir.DIRECTORY_SEPARATOR.$oldXML);


            $cahier->setName($cahierName);
            $cahier->setTeiFileName($teiFileName);
            $cahier->setXmlFileName($splitFileName);
            $message = "Un cahier avec le même nom existe déjà. Son fichier associé a été mis à jour, mais pas de création de nouvelles pages, etc.)";
        }

        $cahierDates = $this->xmlManager->getHTML("dates", $cahier, "cahier");
        $cahier->setDates($cahierDates);

        $this->em->persist($cahier);
        $this->em->flush();

        $this->session->getFlashBag()->add("success", $message);

        return $cahier;
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('#[^\\pL\d]+#u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('#[^-\w]+#', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
