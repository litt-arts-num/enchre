<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class UserManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setRole(User $user, $role)
    {
        $user->setRoles([$role]);
        $this->em->persist($user);
        $this->em->flush();

        return;
    }

    public function create($mail, $password, $passwordEncoder)
    {
        $user = new User();
        $user->setEmail($mail);
        $user->setPassword($passwordEncoder->encodePassword($user, $password));
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
