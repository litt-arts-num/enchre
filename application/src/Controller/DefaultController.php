<?php

namespace App\Controller;

use App\Entity\Cahier;
use App\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }

    /**
     * @Route("/display/{slugName}", name="display_cahier")
     * @ParamConverter("cahier", options={"mapping": {"slugName": "slugName"}})
     */
    public function displayCahier(Cahier $cahier)
    {

        return $this->render('cahier.html.twig', ["cahier" => $cahier]);
    }

    /**
     * @Route("/display/{slugName}/{name}", name="display_page")
     * @ParamConverter("cahier", options={"mapping": {"slugName": "slugName"}})
     * @ParamConverter("page", options={"mapping": {"name": "name", "cahier" : "cahier"}})
     */
    public function displayPage(Cahier $cahier, Page $page)
    {

        return $this->render('page.html.twig', ["page" => $page]);
    }
}
