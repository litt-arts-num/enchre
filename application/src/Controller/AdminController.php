<?php

namespace App\Controller;

use App\Entity\Cahier;
use App\Entity\User;
use App\Form\CahierType;
use App\Form\CommonType;
use App\Form\EditorialType;
use App\Form\ImageType;
use App\Manager\ImportManager;
use App\Manager\UserManager;
use App\Manager\XmlManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function listUsers()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig', [
          "users" => $users
        ]);
    }

    /**
     * @Route("/cahier/{id}/toggle", name="cahier_public_toggle")
     */
    public function toggleCahierVisibility(Cahier $cahier)
    {
        $em = $this->getDoctrine()->getManager();

        $visibility = $cahier->getPublic();
        $cahier->setPublic(!$visibility);
        $em->persist($cahier);
        $em->flush();

        $this->addFlash('success', "La visibilité du cahier a été modifiée");

        return $this->redirectToRoute('display_cahier', ["slugName" => $cahier->getSlugName()]);
    }

    /**
     * @Route("/cahier/{id}/toggle-wip", name="cahier_wip_toggle")
     */
    public function toggleCahierWip(Cahier $cahier)
    {
        $em = $this->getDoctrine()->getManager();

        $wip = $cahier->getWip();
        $cahier->setWip(!$wip);
        $em->persist($cahier);
        $em->flush();

        $this->addFlash('success', "Le cahier a été modifiée");

        return $this->redirectToRoute('display_cahier', ["slugName" => $cahier->getSlugName()]);
    }


    /**
     * @Route("/upload/img", name="upload_img")
     */
    public function uploadImage(ImportManager $importManager, Request $request)
    {
        $uploadDir = $this->getParameter('upload_directory')."images";
        $form =  $this->createForm(ImageType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($file = $form['img']->getData()) {
                $fullFileName = $file->getClientOriginalName();
                $file->move($uploadDir, $fullFileName);

                $this->addFlash('success', "L'image a été transférée'");
            }
        }
        $images = $importManager->getImages($uploadDir);

        return $this->render('admin/upload-image.html.twig', [
          "form" => $form->createView(),
          "images" => $images
        ]);
    }

    /**
     * @Route("/cahier/import-common", name="import_common")
     */
    public function importCommon(Request $request, XmlManager $xmlManager)
    {
        $form =  $this->createForm(CommonType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $xmlManager->cacheInvalidate();
            $uploadDir = $this->getParameter('upload_directory')."common";

            if ($file = $form['notes_annotations_et_noticeBio']->getData()) {
                $fullFileName = $file->getClientOriginalName();
                $file->move($uploadDir, "annotations.xml");

                $this->addFlash('success', "Le fichier 'notes_annotations_et_noticeBio' a été transféré");
            }

            if ($file = $form['dictionnaire']->getData()) {
                $fullFileName = $file->getClientOriginalName();
                $file->move($uploadDir, "dictionnaire.xml");

                $this->addFlash('success', "Le fichier 'dictionnaire' a été transféré");
            }

            if ($file = $form['formes_normalisees']->getData()) {
                $fullFileName = $file->getClientOriginalName();
                $file->move($uploadDir, "formes_normalisees.xml");

                $this->addFlash('success', "Le fichier 'formes_normalisees' a été transféré");
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('admin/import-common.html.twig', [
          "form" => $form->createView()
        ]);
    }


    /**
     * @Route("/cahier/import-edito", name="import_edito")
     */
    public function importEdito(Request $request)
    {
        $form =  $this->createForm(EditorialType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $uploadDir = $this->getParameter('upload_directory')."common";

            if ($file = $form['projet_corpus']->getData()) {
                $file->move($uploadDir, "edito_projet.xml");
                $this->addFlash('success', "Le fichier 'projet_corpus' a été transféré");
            }

            if ($file = $form['principes_edition']->getData()) {
                $file->move($uploadDir, "edito_principes_edition.xml");
                $this->addFlash('success', "Le fichier 'principes_edition' a été transféré");
            }

            if ($file = $form['equipe_partenaires']->getData()) {
                $file->move($uploadDir, "edito_equipe_partenaires.xml");
                $this->addFlash('success', "Le fichier 'equipe_partenaires' a été transféré");
            }

            $uploadDir = $this->getParameter('upload_directory')."pdf";

            if ($file = $form['biblio']->getData()) {
                $file->move($uploadDir, "HR_BIBLIOGRAPHIE.pdf");
                $this->addFlash('success', "Le fichier 'bibliographie' a été transféré");
            }

            if ($file = $form['table_cahiers']->getData()) {
                $file->move($uploadDir, "HR_CAHIERS.pdf");
                $this->addFlash('success', "Le fichier 'table des cahiers' a été transféré");
            }

            if ($file = $form['chronologie']->getData()) {
                $file->move($uploadDir, "HR_CHRONOLOGIE.pdf");
                $this->addFlash('success', "Le fichier 'chronologie' a été transféré");
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('admin/import-edito.html.twig', [
          "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/cahier/import", name="import_cahier")
     */
    public function importCahier(ImportManager $importManager, Request $request, XmlManager $xmlManager)
    {
        $form =  $this->createForm(CahierType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $xmlManager->cacheInvalidate();
            $random = md5(uniqid());
            $uploadDir = $this->getParameter('upload_directory')."transcriptions";
            $oldDir = $this->getParameter('upload_directory')."old";

            if ($file = $form['teiFile']->getData()) {
                $fullFileName = $file->getClientOriginalName();
                $fileName = pathinfo($fullFileName, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $baseName = $random.'-'.$fileName;
                $teiFileName = $baseName.'.'.$extension;
                $file->move($uploadDir, $teiFileName);
            }

            $cahier = $importManager->import($baseName, $teiFileName, $uploadDir, $oldDir);

            return $this->redirectToRoute('display_cahier', ["slugName" => $cahier->getSlugName()]);
        }

        return $this->render('admin/import.html.twig', [
          "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/cahier/{id}/delete", name="delete_cahier")
     */
    public function deleteCahier(Cahier $cahier)
    {   
        $uploadDir = $this->getParameter('upload_directory')."transcriptions";
        $oldDir = $this->getParameter('upload_directory')."old";
        $oldTEI =  $cahier->getTeiFileName();
        $oldXML = $cahier->getXmlFileName();

        $filesystem = new Filesystem;
        $filesystem->rename($uploadDir.DIRECTORY_SEPARATOR.$oldTEI, $oldDir.DIRECTORY_SEPARATOR.$oldTEI);
        $filesystem->rename($uploadDir.DIRECTORY_SEPARATOR.$oldXML, $oldDir.DIRECTORY_SEPARATOR.$oldXML);

        $em = $this->getDoctrine()->getManager();
        $em->remove($cahier);
        $em->flush();

        $this->addFlash('success', "Le cahier a bien été supprimé");

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/user/switch/{id}/{role}", name="set_role")
     */
    public function setRole(User $user, $role, UserManager $um)
    {
        $um->setRole($user, $role);
        $this->addFlash('success', "Changement de rôle effectué");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/user/{id}", name="delete_user")
     */
    public function deleteUser(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', "utilisateur supprimé");

        return $this->redirectToRoute('admin_list_users');
    }
}
