<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:param name="standalone">0</xsl:param>
    <xsl:param name="xml_dir">../../public/upload/common/</xsl:param>

    <xsl:template match="/">
        <xsl:variable name="collection" select="concat($xml_dir, '?select=*.xml')"/>
        <xsl:variable name="tei_file" select="collection($collection)"/>
        <xsl:variable name="entries" select="$tei_file/tei:TEI/tei:text/tei:body//tei:entry"/>
        <xsl:variable name="letters">
            <xsl:text>ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:text>
        </xsl:variable>
        <xsl:for-each select="tokenize($letters, '.')">
            <xsl:if test="position() &lt; last()">
                <xsl:variable name="letter" select="substring($letters, position(), 1)"/>
                <div class="mt-2 letter-block">
                    <div class="card">
                        <div id="notices-bio-{$letter}" class="card-header">
                            <xsl:value-of select="$letter"/>
                        </div>
                        <ul class="list-group list-group-flush text-justify">
                            <xsl:for-each select="$entries[@n = lower-case($letter)]">
                                <xsl:sort select="@xml:id"/>
                                <li id="{@xml:id}" class="notice-entry list-group-item">
                                    <b>
                                        <xsl:value-of select="tei:form"/>
                                    </b>
                                    <xsl:text> </xsl:text>
                                    <i>
                                        <xsl:choose>
                                            <xsl:when test="@type = 'nompropre'">
                                                <xsl:text>n.p.</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="@type = 'nomcommun'">
                                                <xsl:text>n.c.</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                    </i>
                                    <xsl:text> </xsl:text>
                                    <xsl:apply-templates select="tei:sense"/>
                                    <span class="entry-resp">
                                        <xsl:choose>
                                            <xsl:when test="starts-with(@resp, '#')">
                                                <xsl:value-of select="substring-after(@resp, '#')"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="@resp"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </span>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </div>
                </div>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:sense">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:title">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:ref">
        <xsl:choose>
            <xsl:when test="not(. = '')">
                <a href="{@target}">
                    <xsl:apply-templates/>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <a href="{@target}">
                    <xsl:text disable-output-escaping="yes">&lt;i class="fas fa-link">&lt;/i></xsl:text>
                </a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'exp']">
        <sup class="info-link">
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
    <xsl:template match="tei:foreign">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:lb">
        <br/>
    </xsl:template>

    <xsl:template match="*">
        <button class="btn btn-sm bg-warning m-1 p-1">
            <xsl:variable name="message">
                <xsl:value-of select="name(.)"/>
                <xsl:for-each select="@*">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:text>(</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text> </xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>@</xsl:text>
                    <xsl:value-of select="name(.)"/>
                    <xsl:text>=</xsl:text>
                    <xsl:value-of select="."/>
                    <xsl:if test="position() = last()">
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>
            <xsl:value-of select="$message"/>
            <xsl:if test="$message = '1'">
                <xsl:message select="$message"/>
            </xsl:if>
        </button>
    </xsl:template>

</xsl:stylesheet>
