<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:str="http://exslt.org/strings" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:param name="xml_dir">../../public/upload/common/</xsl:param>

    <xsl:template match="tei:persName" mode="pseudodiplo linearized">
        <xsl:param name="normalized_forms" as="node()" tunnel="yes"/>
        
        <xsl:variable name="collection" select="concat($xml_dir, '?select=*.xml')"/>
        <xsl:variable name="tei_file" select="collection($collection)"/>
        <xsl:variable name="entries" select="$tei_file/tei:TEI/tei:text/tei:body//tei:entry"/>
        
        <mark class="enrichment" title="Nom de personne">
            <xsl:if test="not(@ref = '')">
                <xsl:variable name="ref" select="@ref"/>
                <xsl:choose>
                    <xsl:when
                        test="count($normalized_forms/tei:div[1]//*[@xml:id = substring-after($ref, '#')]) = 1">
                        <xsl:attribute name="title" xml:space="default">
                            <xsl:apply-templates
                                select="$normalized_forms/tei:div[1]//*[@xml:id = substring-after($ref, '#')]"
                                mode="html_title"/>
                        </xsl:attribute>
                        <xsl:apply-templates mode="#current"/>
                        <xsl:if test="exists($entries[concat('#',@xml:id) = $ref])">
                            <sup>
                                <a href="/dictionnaire{$ref}">
                                    <xsl:text>i</xsl:text>
                                </a>
                            </sup>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates mode="#current"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </mark>
    </xsl:template>

    <!-- Output a inline string for tei:person content. Useful for HTML @title -->
    <xsl:template match="tei:person" mode="html_title index" xml:space="default">
        <xsl:for-each select="tei:persName">
            <xsl:variable name="out">
                <xsl:apply-templates select="." mode="#current"/>
            </xsl:variable>
            <xsl:value-of select="normalize-space($out)"/>
            <xsl:if test="not(position() = last())">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:persName" mode="html_title">
        <xsl:if test="@type = 'birth'">
            <xsl:value-of select="normalize-space(text()[1])"/>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(*) = 1">
                <xsl:value-of select="."/>
            </xsl:when>
            <xsl:when test="count(*) = 2 and tei:forename and tei:surname">
                <xsl:value-of select="tei:forename"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
            </xsl:when>
            <xsl:when test="count(*) = 3 and tei:forename and tei:surname and tei:nameLink">
                <xsl:value-of select="tei:forename"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:nameLink"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
            </xsl:when>
            <xsl:when test="count(*) = 3 and tei:forename and tei:surname and tei:addName">
                <xsl:value-of select="tei:forename"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
                <xsl:text> dit </xsl:text>
                <xsl:value-of select="tei:addName"/>
            </xsl:when>
            <xsl:when test="count(*) = 3 and tei:forename and tei:surname and tei:roleName">
                <xsl:value-of select="tei:forename"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:roleName"/>
            </xsl:when>
            <xsl:when
                test="count(*) = 3 and tei:forename[@type = 'first'] and tei:forename[@type = 'middle'] and tei:surname">
                <xsl:value-of select="tei:forename[@type = 'first']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:forename[@type = 'middle']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
            </xsl:when>
            <xsl:when
                test="count(*) = 4 and tei:forename and tei:nameLink and tei:surname and tei:roleName">
                <xsl:value-of select="tei:forename"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:nameLink"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:roleName"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message>
                    <xsl:text>oups : </xsl:text>
                    <xsl:value-of select="../@xml:id"/>
                    <xsl:text> (</xsl:text>
                    <xsl:for-each select="*">
                        <xsl:sort select="name()"/>
                        <xsl:value-of select="concat(name(), ' ')"/>
                    </xsl:for-each>
                    <xsl:text>)]</xsl:text>
                </xsl:message>
                <xsl:text>[</xsl:text>
                <xsl:value-of select="normalize-space(.)"/>
                <xsl:text>]</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:persName" mode="index">
        <xsl:if test="@type = 'birth'">
            <xsl:value-of select="normalize-space(text()[1])"/>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(*) = 1">
                <xsl:value-of select="."/>
            </xsl:when>
            <xsl:when test="count(*) = 2 and tei:forename and tei:surname">
                <xsl:value-of select="tei:surname"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:forename"/>
            </xsl:when>
            <xsl:when test="count(*) = 3 and tei:forename and tei:surname and tei:nameLink">
                <xsl:value-of select="tei:surname"/>
                <xsl:text> (</xsl:text>
                <xsl:value-of select="tei:nameLink"/>
                <xsl:text>)</xsl:text>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:forename"/>
            </xsl:when>
            <xsl:when test="count(*) = 3 and tei:forename and tei:surname and tei:addName">
                <xsl:value-of select="tei:surname"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:forename"/>
                <xsl:text> dit </xsl:text>
                <xsl:value-of select="tei:addName"/>
            </xsl:when>
            <xsl:when test="count(*) = 3 and tei:forename and tei:surname and tei:roleName">
                <xsl:value-of select="tei:surname"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:forename"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:roleName"/>
            </xsl:when>
            <xsl:when
                test="count(*) = 3 and tei:forename[@type = 'first'] and tei:forename[@type = 'middle'] and tei:surname">
                <xsl:value-of select="tei:surname"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:forename[@type = 'first']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:forename[@type = 'middle']"/>
            </xsl:when>
            <xsl:when
                test="count(*) = 4 and tei:forename and tei:nameLink and tei:surname and tei:roleName">
                <xsl:value-of select="tei:surname"/>
                <xsl:text> (</xsl:text>
                <xsl:value-of select="tei:nameLink"/>
                <xsl:text>)</xsl:text>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:forename"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:roleName"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message>
                    <xsl:text>oups : </xsl:text>
                    <xsl:value-of select="../@xml:id"/>
                    <xsl:text> (</xsl:text>
                    <xsl:for-each select="*">
                        <xsl:sort select="name()"/>
                        <xsl:value-of select="concat(name(), ' ')"/>
                    </xsl:for-each>
                    <xsl:text>)]</xsl:text>
                </xsl:message>
                <xsl:text>[</xsl:text>
                <xsl:value-of select="normalize-space(.)"/>
                <xsl:text>]</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:addName" mode="index">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

    <xsl:template match="tei:seg[@ana]" mode="#all">
        <span data-id-notion="{@ana}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:ref[not(@target)]" mode="#all"/>

</xsl:stylesheet>
