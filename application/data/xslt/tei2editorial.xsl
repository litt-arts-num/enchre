<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of TEI file to HTML editorial pages
* @author AnneGF@CNRS
* @date : 2022-2023
* Usage: give the html web content looking (into $xml-dir folder) for a file with //tei:text/@xml-id equal to the (final) slug of the webpage.
  * "slug" of the page (e.g. "home")
  * xml-dir (folder with xml-tei files) 
  * [optional] "lang" (default 'false' for non-multi-lingual website.
    If provided, the final slug will be $slug_$lang (e.g. "home_fr")
**/
-->
<!DOCTYPE tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:sf="http://saxon.sf.net/" xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs tei sf" version="2.0">

    <xsl:param name="slug">principes-edition</xsl:param>
    <xsl:param name="lang">
        <xsl:value-of select="false()"/>
    </xsl:param>
    <xsl:param name="xml-dir">../../public/upload/common/</xsl:param>

    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/">
        <xsl:variable name="collection" select="concat($xml-dir, '?select=*.xml')"/>
        <xsl:variable name="tei_file" select="collection($collection)"/>
        <xsl:variable name="xml_id">
            <xsl:choose>
                <xsl:when test="not($lang = false())">
                    <xsl:value-of select="concat($slug, '_', $lang)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$slug"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <div class="col">
            <xsl:choose>
                <xsl:when test="count($tei_file//tei:text[@xml:id = $xml_id]) = 1">
                    <xsl:apply-templates select="$tei_file//tei:text[@xml:id = $xml_id]"/>
                </xsl:when>
                <xsl:otherwise>
                    <p>
                        <i>Page en construction</i>
                    </p>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <xsl:template match="tei:text">
        <xsl:choose>
            <xsl:when test="not($lang = false())">
                <xsl:apply-templates
                    select="../tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@xml:lang = $lang]"
                />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="../tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"
                />
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="name(tei:body/child::*[1]) = 'p'">
                <p class="chapeau">
                    <xsl:apply-templates select="tei:body/child::*[1]" mode="inside"/>
                </p>
                <xsl:apply-templates select="tei:body/child::*[position() > 1]"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="tei:body/child::*"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:div[@synch]">
        <xsl:variable name="synch" select="@synch"/>
        <xsl:apply-templates select="/tei:TEI//tei:*[concat('#', @xml:id) = $synch]"/>
    </xsl:template>
    <xsl:template match="tei:div">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:p" mode="inside">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:lb">
        <br/>
    </xsl:template>
    <xsl:template match="tei:title[parent::tei:titleStmt]">
        <h1>
            <xsl:apply-templates/>
        </h1>
    </xsl:template>
    <xsl:template match="tei:title">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:head">
        <xsl:element name="h3">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'bold']">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'underline']">
        <u>
            <xsl:apply-templates/>
        </u>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:figure">
        <img alt="{tei:figDesc}" src="{tei:graphic/@url}"/>
    </xsl:template>
    <xsl:template match="tei:email">
        <mark>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    <xsl:template match="*">
        <b style="color:red">
            <xsl:value-of select="name(.)"/>
        </b>
    </xsl:template>
</xsl:stylesheet>
