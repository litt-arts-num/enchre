var Encore = require('@symfony/webpack-encore');

Encore
  // directory where compiled assets will be stored
  .setOutputPath('public/build/')
  .setPublicPath('/build')
  .addEntry('js/main', './assets/js/main.js')
  .addEntry('js/filter', './assets/js/filter.js')
  .addEntry('js/display-page', './assets/js/display-page.js')
  .addEntry('js/cookie', './assets/js/cookie.js')
  .addStyleEntry('css/template', './assets/css/template.css')
  .addStyleEntry('css/project', './assets/css/project.css')
  .addStyleEntry('css/elan-tei', './assets/css/elan-tei.css')
  .addStyleEntry('css/HR_style', './assets/css/HR_style.css')
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning()
  .createSharedEntry('js/vendor', './assets/js/vendor.js')
  .autoProvidejQuery()
  .autoProvideVariables({
    bsCustomFileInput: 'bs-custom-file-input'
  });

module.exports = Encore.getWebpackConfig();
