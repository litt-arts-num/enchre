FROM ubuntu:20.04

MAINTAINER Arnaud Bey

RUN apt-get -qqq update && DEBIAN_FRONTEND=noninteractive apt-get install -qqq -y \
        apt-utils \
        git \
        make \
        vim \
        wget \
        curl \
        gdebi \
        apache2 \
        openssl \
        php7.4 \
        libapache2-mod-php7.4 \
        php7.4-bcmath \
        php7.4-cli \
        php7.4-common \
        php7.4-curl \
        php7.4-dev \
        php7.4-fpm \
        php7.4-intl \
        php7.4-json \
        php7.4-mbstring \
        php7.4-mysql \
        php7.4-opcache \
        php7.4-readline \
        php7.4-xml \
        php7.4-zip \
        php7.4-gd \
        php-common \
        php-imagick \
        php7.4-xsl \
        unzip


# Install Composer
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer && chmod +x /usr/local/bin/composer

# Install npm
RUN curl -o /usr/local/bin/n https://raw.githubusercontent.com/visionmedia/n/master/bin/n
RUN chmod +x /usr/local/bin/n
RUN n stable

RUN mkdir /var/www/public

# Configure Apache
RUN rm -rf /etc/apache2/sites-available/* && rm -rf /etc/apache2/sites-enabled/*

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
ADD ./app.conf /etc/apache2/sites-available/app.conf

RUN a2ensite app.conf
RUN a2enmod headers
RUN a2enmod deflate
RUN a2enmod rewrite
RUN a2enmod ssl
RUN service apache2 restart

##### SAXON
RUN wget http://www.saxonica.com/saxon-c/libsaxon-HEC-setup64-v1.1.2.zip && unzip libsaxon-HEC-setup64-v1.1.2.zip
RUN yes "" | ./libsaxon-HEC-setup64-v1.1.2
RUN ln -s /usr/lib/ /usr/lib64
RUN ln -s /Saxonica/Saxon-HEC1.1.2/libsaxonhec64.so /usr/lib/libsaxonhec64.so
RUN ln -s /Saxonica/Saxon-HEC1.1.2/rt/ /usr/lib/rt
RUN export LD_LIBRARY_PATH=/usr/lib/rt/lib/amd64:$LD_LIBRARY_PATH
RUN export SAXONC_HOME=/Saxonica/Saxon-HEC1.1.2/

RUN ldconfig

WORKDIR /Saxonica/Saxon-HEC1.1.2/Saxon.C.API/

RUN wget https://saxonica.plan.io/projects/saxon/repository/he/revisions/master/raw/latest9.8/hec/Saxon.C.API/jni/jni.h
RUN wget https://saxonica.plan.io/projects/saxon/repository/he/revisions/master/raw/latest9.8/hec/Saxon.C.API/jni/unix/jni_md.h

RUN phpize && ./configure --enable-saxon CPPFLAGS="-Ijni" && make && make install
RUN echo extension=saxon.so >> /etc/php/7.4/apache2/conf.d/saxon.ini
RUN echo extension=saxon.so >> /etc/php/7.4/mods-available/saxon.ini
RUN phpenmod saxon
RUN php -m | grep saxon -i
RUN echo export LD_LIBRARY_PATH=/usr/lib/rt/lib/amd64:$LD_LIBRARY_PATH >> /etc/apache2/envvars
# CMD ["apachectl", "-DFOREGROUND"]

RUN sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.4/apache2/php.ini
RUN sed -i "s/error_reporting = .*$/error_reporting = E_ERROR | E_WARNING | E_PARSE/" /etc/php/7.4/apache2/php.ini

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV SAXONC_HOME /Saxonica/Saxon-HEC1.1.2/
##############################

RUN echo 'alias console="php bin/console"' >> ~/.bashrc

WORKDIR /var/www/

# as www-data user
RUN usermod -u 1000 www-data

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
